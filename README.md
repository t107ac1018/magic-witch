# Magic witch

110-1行動裝置遊戲設計實務-期末專案
組員：107AC1018潘娪婷、107AC1019姜錦蓉

# 專案簡述
PC雙人連線+AR呈現
兩位玩家扮演魔女，控制魔法石，一起合作啟動魔法陣，召喚出魔女的動物使魔
但是魔女被詛咒了，只能控制另外一位魔女的視角
需要雙方合作溝通，才能完成偉大的召喚術


# 操作方法
雙人合作，使用鍵盤方向鍵操控小球；手機開啟AR掃描查看召喚成果
不同的排列組合與視角，皆會召喚不同生物
生物共有12種，召喚機率不一，一起來喚出所有的使魔吧。

# 執行環境
專案Unity版本- 2019.1.1f1
PC-windows10
手機-Andriod系統

# 安裝說明
分別在PC下載「PC game_Magic witch」，手機下載「89aok1080.apk」，開啟後輸入遊玩者名稱便可遊玩。

# 參考文獻
[Photon 社群小聚 (2019.08) 活動記錄. (Roll-a-ball 多人連線版本) | by Ryan Tseng | Photon Taiwan | Medium](https://medium.com/photon-taiwan/photon-%E7%A4%BE%E7%BE%A4%E5%B0%8F%E8%81%9A-2019-08-%E6%B4%BB%E5%8B%95%E8%A8%98%E9%8C%84-6a1fccda045c)

# 運行截圖
[預覽](https://gitlab.com/t107ac1018/magic-witch/-/tree/main/%E8%AA%AA%E6%98%8E%E6%96%87%E4%BB%B6/image)
