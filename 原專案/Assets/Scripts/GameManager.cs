﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Com.ABCDE.RollABall
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        [Tooltip("Prefab- 玩家的角色")]
        public GameObject playerPrefab;
        // 玩家離開遊戲室時, 把他帶回到遊戲場入口
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }
        void Start()
        {
        if (playerPrefab == null)
            {
                Debug.LogError("playerPrefab 遺失", this);
            }
            else
            {
                Debug.LogFormat("我們從 {0} 實例化玩家角色",
                    SceneManagerHelper.ActiveSceneName);
                Debug.LogFormat("playerPrefab 的 name 為 {0} ",
                    this.playerPrefab.name);

                PhotonNetwork.Instantiate(this.playerPrefab.name,
                    new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
            }
        }
        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }
        void LoadArena()
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                Debug.LogError("我不是 Master Client, 不做載入場景的動作");
            }
            Debug.Log("載入 MiniGame 遊戲場景");
            PhotonNetwork.LoadLevel("MiniGame");
        }
    }
}