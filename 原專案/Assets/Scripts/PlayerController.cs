﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Pun.Demo.PunBasics;

namespace Com.ABCDE.RollABall
{ 
public class PlayerController : MonoBehaviourPunCallbacks
    {
    public float speed;
    //public Text countText;
    //public Text winText;

    private Rigidbody rb;
    public int count;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        //SetCountText();
        //winText.text = "";
        CameraWork _cameraWork =
            this.gameObject.GetComponent<CameraWork>();
        if (_cameraWork != null)
        {
            if (photonView.IsMine)
            {
                _cameraWork.OnStartFollowing();
            }
        }
        else
        {
            Debug.LogError("playerPrefab- CameraWork component 遺失",
                this);
        }
    }

    void FixedUpdate()
    {
            if (!photonView.IsMine)
            {
                return;
            }
            float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

    }

    void OnTriggerEnter(Collider other)
    {
            if (!photonView.IsMine)
            {
                return;
            }
            if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
           // SetCountText();
        }
    }

        /*void SetCountText()
        {
            countText.text = "Count: " + count.ToString();
            if (count >= 12)
            {
                winText.text = "You win!";
            }
        }*/
        public void OnPhotonSerializeView(PhotonStream stream,
        PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(count);
            }
            else
            {
                this.count = (int)stream.ReceiveNext();
            }
        }
       
    }
}